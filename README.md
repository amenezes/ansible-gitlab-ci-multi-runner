ansible-gitlab-ci-multi-runner
---
[![build status](https://gitlab.com/amenezes/ansible-gitlab-ci-multi-runner/badges/master/build.svg)](https://gitlab.com/amenezes/ansible-gitlab-ci-multi-runner/commits/master)

Ansible playbook para instalar e configurar o [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner).


Role Variables
---

Veja a relação completa em: [defaults/main.yml](defaults/main.yml)


Exemplo
---

arquivo de configuração
```yml
- hosts: servers
  roles:
    - role: '{{playbook_dir}}'
    gitlab_ci_multi_runner_activate: False
    gitlab_ci_multi_runner_addl_groups: []
```

linha de comando
```bash
ansible-playbook -i inventory test.yml --syntax-check
```

Configurações do Runner
---
- [Gitlab Runner](https://docs.gitlab.com/runner/)
- [Instalação](https://docs.gitlab.com/runner/install/index.html)
- [Executor](https://docs.gitlab.com/runner/executors/README.html)
- [config.toml referência](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)
- [Gitlab Runner Commands](https://docs.gitlab.com/runner/commands/README.html)
- [Registering Runners](https://docs.gitlab.com/runner/register/index.html#docker)
- [Configuration of your jobs with .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)
- [Using Docker Build](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html)


repositório original: Andrew Rothstein <andrew.rothstein@gmail.com>
