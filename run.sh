#!/usr/bin/env bash

menu_ajuda()
{
  echo ">> $0"
  echo "Uso:"
  echo "bash $0"
  echo ""
  echo "Opcoes:"
  echo "-h                 exibe este menu de ajuda"
  echo "-c PLAYBOOK_FILE   efetua verificacao do playbook especificado (ansible check)"
  echo "-s PLAYBOOK_FILE   inicia o provisionamenot do playbook especificado"
}

while getopts "s:c:" opt; do
  case $opt in
  c)
    echo "[*] iniciando verificacao..."
    ansible-playbook -i inventory -k $OPTARG -c paramiko -C
    ;;
  s)
    echo "[*] iniciando provisionamento..."
    ansible-playbook -i inventory -k $OPTARG -c paramiko
    ;;
  *)
    menu_ajuda
    ;;
  esac
done

if [ -z "${s}" ] || [ -z "${c}" ]
then
  menu_ajuda
fi
